class TodoListCard extends HTMLElement {
    constructor() {
      super();
      this._hass = null;
      this._entities = [];
    }
  
    set hass(hass) {
      this._hass = hass;
      if (!this.content) {
        const card = document.createElement('ha-card');
        card.header = 'Todo List';
  
        this.content = document.createElement('div');
        card.appendChild(this.content);
        this.appendChild(card);
  
        this.content.innerHTML = `
          <div>
            <label for="list">Select Todo List:</label>
            <select id="list">
              ${this.config.lists.map(list => `<option value="${list}">${list}</option>`).join('')}
            </select>
          </div>
          <div>
            <label for="numTodos">Number of Todos to show:</label>
            <input type="number" id="numTodos" min="1" value="5">
          </div>
          <div>
            <label for="filter">Filter by:</label>
            <select id="filter">
              <option value="priority">Priority</option>
              <option value="due_date">Due Date</option>
              <option value="added_date">Added Date</option>
              <option value="random">Random</option>
            </select>
          </div>
          <div id="todoList"></div>
        `;
  
        this.content.querySelector('#list').addEventListener('change', () => this.updateTodoList());
        this.content.querySelector('#numTodos').addEventListener('input', () => this.updateTodoList());
        this.content.querySelector('#filter').addEventListener('change', () => this.updateTodoList());
        
        this.updateTodoList();
  
        this._setupListeners();
      }
    }
  
    _setupListeners() {
      const list = this.content.querySelector('#list').value;
      const entityId = `sensor.${list}_todos`;
  
      if (!this._entities.includes(entityId)) {
        this._entities.push(entityId);
      }
  
      // Listen for state changes
      this._hass.connection.subscribeEntities(entities => {
        if (this._entities.some(entity => entities[entity] && entities[entity].state !== this._hass.states[entity].state)) {
          this.updateTodoList();
        }
      });
    }
  
    async updateTodoList() {
      const list = this.content.querySelector('#list').value;
      const numTodos = this.content.querySelector('#numTodos').value;
      const filter = this.content.querySelector('#filter').value;
      const todoListElement = this.content.querySelector('#todoList');
  
      console.log(`Updating todo list: ${list}, numTodos: ${numTodos}, filter: ${filter}`);
  
      try {
        const todos = await this.fetchTodos(list);
  
        let filteredTodos;
        switch (filter) {
          case 'priority':
            filteredTodos = todos.sort((a, b) => a.priority - b.priority);
            break;
          case 'due_date':
            filteredTodos = todos.sort((a, b) => new Date(a.due_date) - new Date(b.due_date));
            break;
          case 'added_date':
            filteredTodos = todos.sort((a, b) => new Date(a.added_date) - new Date(b.added_date));
            break;
          case 'random':
            filteredTodos = todos.sort(() => 0.5 - Math.random());
            break;
          default:
            filteredTodos = todos;
        }
  
        filteredTodos = filteredTodos.slice(0, numTodos);
  
        todoListElement.innerHTML = `
          <ul>
            ${filteredTodos.map(todo => `<li>${todo.title} - ${todo.due_date ? `Due: ${todo.due_date}` : ''} - ${todo.priority ? `Priority: ${todo.priority}` : ''}</li>`).join('')}
          </ul>
        `;
      } catch (error) {
        console.error('Error fetching todos:', error);
        todoListElement.innerHTML = `<p>Error fetching todos.</p>`;
      }
    }
  
    async fetchTodos(list) {
      return new Promise((resolve, reject) => {
        this._hass.callService('todo', 'get_items', { list, return_response: true }, result => {
          if (result.success) {
            resolve(result.response.items);
          } else {
            reject(new Error('Failed to fetch todos'));
          }
        });
      });
    }
  
    setConfig(config) {
      if (!config.lists) {
        throw new Error('You need to define a list of todo lists.');
      }
      this.config = config;
      console.log('TodoListCard: config set', config);
    }
  
    getCardSize() {
      return 3;
    }
  }
  
  customElements.define('todo-list-card', TodoListCard);
  